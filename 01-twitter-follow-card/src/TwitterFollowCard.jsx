import React, { Children, useState } from 'react'

const TwitterFollowCard = ({children, username, initialIsFollowing}) => {

    const [ isFollowing, setIsFollowing] = useState(initialIsFollowing);



  const handleClick = () => {
    setIsFollowing(!isFollowing);
  }

  const buttonClassName = isFollowing?   'tw-followCard-button is-following'
  : 'tw-followCard-button';

  const text = isFollowing ? 'Siguiendo': 'Seguir';

  return (
    <article className='tw-followCard'>
        <header className='tw-followCard-header'>
            <img 
                className='tw-followCard-avatar'
                src={`https://unavatar.io/${username}`} alt={username} />
            <div className='tw-followCard-info'>
                <strong>{children}</strong>
                <span className='tw-followCard-infouserName'>@{username}</span>
            </div>
        </header>
        <aside>
            <button className={buttonClassName} onClick={handleClick}>
                <span className='tw-followCard-text'>{text}</span>
                <span className='tw-followCard-stopFollow'>Dejar de seguir</span>
            </button>
        </aside>
    </article>
  )
}

export default TwitterFollowCard