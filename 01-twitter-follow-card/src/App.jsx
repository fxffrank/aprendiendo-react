import './App.css'
import TwitterFollowCard from './TwitterFollowCard'

const users = [
  {
    userName: 'midudev',
    name: 'Miguel Ángel Durán',
    isFollowing: true
  },
  {
    userName: 'pheralb',
    name: 'Pablo H.',
    isFollowing: false
  },
  {
    userName: 'PacoHdezs',
    name: 'Paco Hdez',
    isFollowing: true
  },
  {
    userName: 'TMChein',
    name: 'Tomas',
    isFollowing: false
  },
  {
    userName: 'Frank_FloresM',
    name: 'Frank Flores',
    isFollowing: false
  }
]

function App() {
  
  return (
    <section className='App'>
    {
      users.map(({userName, name, isFollowing}) => {
        return <TwitterFollowCard key={userName} username={userName} initialIsFollowing={isFollowing}>
          {name}
        </TwitterFollowCard>
      })
      
}
    </section>
  )
}

export default App
