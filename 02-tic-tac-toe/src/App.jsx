import { useState } from 'react'
import './App.css'
import { Square } from './components/Square.jsx'
import { TURNS } from './constants'
import { WinnerModal } from './components/WinnerModal';
import { checkEndGame, checkWinner } from './logic/board';
import { resetGameStorage, saveGameStorage } from './logic/storage';


function App() {

  const [board, setBoard] = useState(() => {
    const boardFromStorage = window.localStorage.getItem('board');
    console.log(boardFromStorage);
    if (boardFromStorage) {
      return JSON.parse(boardFromStorage);
    } 
    return Array(9).fill(null)});

  const [turn, setTurn] = useState(()=>{
    const turnFromStorage = window.localStorage.getItem('turn');
    return turnFromStorage ?? TURNS.X});

  const [winner, setWinner] = useState(null);

  const resetGame = () =>{
    setBoard(Array(9).fill(null));
    setTurn(TURNS.X);
    setWinner(null)
    resetGameStorage();
  }

  const updateBoard = (index) => {
    if (board[index] || winner) return;

    const newBoard = [...board];
    newBoard[index] = turn;
    setBoard(newBoard)

    const newTurn = turn === TURNS.X ? TURNS.O : TURNS.X;
    setTurn(newTurn)

    saveGameStorage({
      board: newBoard,
      turn: newTurn
    });

    //check if there is a winner
    const newWinner = checkWinner(newBoard);
    if (newWinner){
      setWinner(newWinner);
    }
    
    if (checkEndGame(newBoard)) {
      setWinner(false);
    }
    
  }
  

  return (
    <main className='board'>
      <h1>Tic Tac Toe</h1>
      <p>Hola Mundo</p>
      <button onClick={resetGame}>Reset game</button>
      <section className='game'>
        {
          board.map((square, index) => {
            return (<Square key={index} updateBoard={updateBoard} index={index}>
              {square}
            </Square>)
          })
        }
      </section>
      <section className='turn'>
        <Square isSelected={turn === TURNS.X}>
          {TURNS.X}
        </Square>
        <Square isSelected={turn === TURNS.O}>
          {TURNS.O}
        </Square>
      </section>
      <WinnerModal resetGame={resetGame} winner={winner}/>
    </main>
  )
}

export default App
